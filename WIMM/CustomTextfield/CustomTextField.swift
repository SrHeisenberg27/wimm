//
//  CustomTextField.swift
//  WIMM
//
//  Created by Álvaro Ferrández Gómez on 30/05/2020.
//  Copyright © 2020 Álvaro Ferrández Gómez. All rights reserved.
//

import UIKit

@IBDesignable open class CustomTextField: UIView {

    @IBOutlet weak var contentViewTextfield: UIView!
    @IBOutlet weak var lineTextfield: UIView!
    @IBOutlet weak var customTextfield: UITextField!
    @IBOutlet weak var titleLabel: UILabel!

    open var validCharacters = "qwertyuiopasdfghjklñzxcvbnm@1234567890.-_"

    open override func awakeFromNib() {
        super.awakeFromNib()
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }

    fileprivate func xibSetup() {
        contentViewTextfield = loadViewFromNib()
        contentViewTextfield.frame = bounds
        contentViewTextfield.translatesAutoresizingMaskIntoConstraints = true
        addSubview(contentViewTextfield)
    }

    fileprivate func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "CustomTextField", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as? UIView
        return view ?? UIView()
    }

    public func setupCustomTextField(title: String) {
        self.titleLabel.text = title
        
        self.setupUI()

        self.customTextfield.delegate = self
    }

    fileprivate func setupUI() {
        self.customTextfield.backgroundColor = .clear
        self.lineTextfield.backgroundColor = WIMMUtils.corporativeRed
    }
}

extension CustomTextField: UITextFieldDelegate {
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let textFieldText = textField.text else { return }

    }

    public func textFieldDidEndEditing(_ textField: UITextField) {
        guard let textFieldText = textField.text else { return }

    }

    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text else {
            return false
        }
        guard range.length + range.location <= textFieldText.count else {
            let nsString = textField.text as NSString?
            let newString = nsString?.replacingCharacters(in: range, with: string)

            return false
        }
        for char in string {
            guard validCharacters.contains(String(char)) else {
                let nsString = textField.text as NSString?
                let newString = nsString?.replacingCharacters(in: range, with: string)

                return false
            }
        }

        let newLenght = textFieldText.count + string.count - range.length
        let nsString = textField.text as NSString?
        let newString = nsString?.replacingCharacters(in: range, with: string)

        textField.text = newString

        return false
    }
}
